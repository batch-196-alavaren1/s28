// db.users.insertOne({
// 
// 
//             "username": "dahyunTwice",
// 
//             "password": "dahyunKim"
// 
// })
// 
// db.users.insertOne({
// 
// 
//             "username": "gokuSon",
// 
//             "password": "over9000"
// })

// 

// insert multiple document at once

// db.users.insertMany(
//         [
//             {
//                  "username": "pablo123",
// 
//                   "password": "123paul" 
//             },
//             {
//                  "username": "pedro99",
// 
//                   "password": "iampeter99" 
//             }
//             
//         
//         ]
//         
//         )

// db.products.insertOne(
// {
// 
// 
//             "name": "cellphone",
// 
//             "description": "samsung",
//             
//             "price": 27000
// 
// })

// Read/Retrieve


// db.users.find() - return/find all documents in the sollection.

// db.users.find()


// db.collection.find({"criteria":"value"}) - return/find all documents that match criteria

// db.users.find({"username":"pedro99"})

// db.cars.insertMany (
//     [
//         {
//           "name": "Vios",
//           "brand": "Toyota",
//             "type": "sedan",
//             "price":"150000"
//          }, 
//           {
//           "name": "City",
//           "brand": "honda",
//             "type": "auv",
//             "price":"750000"
//          }, 
//           {
//           "name": "Tamaraw FX",
//           "brand": "Toyota",
//             "type": "sedan",
//             "price":"1600000"
//          },
//      ]
//  )
//  
//  db.cars.find({"type":"sedan"})

// db.cars.findOne({}) - find/return the first item

// db.cars.findOne({"type":"sedan"}) - the first item/document that matches the criteria
// 
// db.cars.findOne({"brand":"Toyota"})
// db.cars.findOne({"brand":"honda"})




// UPDATE
//db.collection.updateOne({})
// allows uo to update the first item that matches uor criteria
// db.users.updateOne({"username":"pedro99"},{$set:{"username":"peter199"}})


//  db.users.updateOne({},{$set:{"username":"updatedUsername"}})
//  
//  db.users.updateOne({"username":"pablo123"},{$set:{"isAdmin":true}}) - If the field being updated does not exist, mongodb will instead 
//                                                                      - add that field into document
// 

    

// db.users.updateMany({},{$set:{"isAdmin":true}}) - Allows up to update all items in the collection.
    
// db.cars.updateMany({"type":"sedan"},{$set:{"price":100000000}}) - Allows us to update all items that match uor criteria




// DELETE //

// db.products.deleteOne({}) - deletes the first item in collection

// 
// db.cars.deleteOne({"brand":"Toyota"}) -delete the first item that matches the criteria
 
// db.users.deleteMany({"isAdmin":true}) - delete all items that matches the criteria

// db.collection.deleteMany({})
// delete all document in a collection
// db.products.deleteMany({})
// db.cars.deleteMany({}) 
    

    